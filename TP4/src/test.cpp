#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <vector>
#include "Ligne.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"

#include <iostream>

TEST_CASE("test_point", "test 1") {
    Point p1(1, 2);
    REQUIRE(p1._x== 1);
    REQUIRE(p1._y== 2);
}

TEST_CASE("test_couleur", "test 1") {
    Couleur c(150, 180, 210);
    REQUIRE(c._r== 150);
    REQUIRE(c._g== 180);
    REQUIRE(c._b== 210);
}

TEST_CASE("test_ligne", "test 1") {
    Couleur c(150, 180, 210);
    Point p1(1, 2);
    Point p2(3, 4);
    Ligne l(c, p1, p2);

    REQUIRE(l.getP0()._x == 1);
    REQUIRE(l.getP0()._y == 2);
    REQUIRE(l.getP1()._x == 3);
    REQUIRE(l.getP1()._y == 4);

    REQUIRE(l.getCouleur()._r== 150);
    REQUIRE(l.getCouleur()._g== 180);
    REQUIRE(l.getCouleur()._b== 210);

}

TEST_CASE("test_polygone", "test 1") {
    Couleur c(150, 180, 210);
    Point p1(1, 2);
    Point p2(3, 4);
    PolygoneRegulier po(c, p1, 4, 4);

    REQUIRE(po.getCouleur()._r== 150);
    REQUIRE(po.getCouleur()._g== 180);
    REQUIRE(po.getCouleur()._b== 210);

    REQUIRE(po.getPoint(0)._x == 5);
    REQUIRE(po.getPoint(0)._y == 2);

    REQUIRE(po.getNbPoints() == 4);

}
