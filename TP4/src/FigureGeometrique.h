//
// Created by Arthur on 23/03/2020.
//

#ifndef TP_VIDE_FIGUREGEOMETRIQUE_H
#define TP_VIDE_FIGUREGEOMETRIQUE_H

#include "Couleur.h"

class FigureGeometrique {

protected:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur &couleur);
    Couleur getCouleur() const;
    virtual void afficher() const;

};



#endif //TP_VIDE_FIGUREGEOMETRIQUE_H
