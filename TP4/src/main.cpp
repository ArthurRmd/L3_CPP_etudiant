
#include <iostream>

#include <vector>
#include "Ligne.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"

int main() {

    Couleur c(160, 160, 160);
    Point p1(1, 1);
    Point p2(5, 5);

    Ligne l(c, p1, p2);
    l.afficher();


    /*****/
    PolygoneRegulier po(c, p1, 4, 4);
    po.afficher();

    std::cout << "--------------" << std::endl;


    std::vector<FigureGeometrique *> tab = {
            new Ligne(c, p1, p2),
            new PolygoneRegulier(c, p1, 4, 4)
    };

    for (FigureGeometrique *fg : tab) {
        fg->afficher();
    }


    return 0;
}

