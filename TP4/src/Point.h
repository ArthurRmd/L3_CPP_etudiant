//
// Created by Arthur on 23/03/2020.
//

#ifndef TP_VIDE_POINT_H
#define TP_VIDE_POINT_H

struct Point {
    int _x;
    int _y;

    Point(){}

    Point(int x, int y) {
        _x = x;
        _y = y;
    }

    void afficher() const {
        std::cout << "(" << _x << ";" << _y << ")" <<  std::endl;

    }
};

#endif //TP_VIDE_POINT_H
