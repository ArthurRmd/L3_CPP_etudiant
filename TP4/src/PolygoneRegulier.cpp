//
// Created by Arthur on 23/03/2020.
//

#include <cmath>
#include <iostream>
#include "PolygoneRegulier.h"
#include "Point.h"

using namespace std;

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, Point &centre, int rayon, int nbCotes) :
        FigureGeometrique(couleur)  {

    _nbPoints = nbCotes;
    _points = new Point[_nbPoints];

    for (int i = 0; i < _nbPoints; i++) {
        float theta = i * 2 * 3.14 / (float) _nbPoints;
        int x = centre._x + rayon * cos(theta);
        int y = centre._y + rayon * sin(theta);
        _points[i] = Point(x, y);

    }




}


void PolygoneRegulier::afficher() const {

    std::cout << "Polygone : " << _nbPoints << " points, voici les points :" <<   std::endl;
    for (int i = 0; i < _nbPoints ; ++i) {
        _points[i].afficher();
    }

}


int PolygoneRegulier::getNbPoints() const {
    return _nbPoints;
}

const Point &PolygoneRegulier::getPoint(int indice) const {
    return _points[indice];
}
