//
// Created by Arthur on 23/03/2020.
//

#ifndef TP_VIDE_COULEUR_H
#define TP_VIDE_COULEUR_H

#include <iostream>

struct Couleur {
    double _r;
    double _g;
    double _b;

    Couleur(double r, double g, double b) {
        _r = r;
        _g = g;
        _b = b;
    }

    void afficher() const {
        std::cout << _r << "-"
                  << _g << "-"
                  << _b  << std::endl;

    }
};



#endif //TP_VIDE_COULEUR_H
