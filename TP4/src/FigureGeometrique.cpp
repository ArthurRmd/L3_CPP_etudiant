//
// Created by Arthur on 23/03/2020.
//

#include "Couleur.h"
#include "FigureGeometrique.h"


FigureGeometrique::FigureGeometrique(const Couleur &couleur) :
        _couleur(couleur) {}


Couleur FigureGeometrique::getCouleur() const {
    return _couleur;
}

void FigureGeometrique::afficher() const {
    std::cout << "je suis une figure geometrique " << std::endl;
}