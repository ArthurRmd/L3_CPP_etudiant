//
// Created by Arthur on 23/03/2020.
//

#ifndef TP_VIDE_POLYGONEREGULIER_H
#define TP_VIDE_POLYGONEREGULIER_H

#include "Point.h"
#include "FigureGeometrique.h"

class PolygoneRegulier : public FigureGeometrique {

private:
    int _nbPoints;
    Point *_points;

public:
    PolygoneRegulier(const Couleur &couleur, Point &centre, int rayon, int nbCotes);
    int getNbPoints() const;
    const Point &getPoint(int indice) const;
    virtual void afficher() const override;

};


#endif //TP_VIDE_POLYGONEREGULIER_H
