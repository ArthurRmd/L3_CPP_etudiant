#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    chargerInventaire("/home/arthur/Documents/C++/L3_CPP_etudiant/TP8/mesBouteilles.txt");

    for (auto &v : _vues) {
        v->actualiser();
    }
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}


std::string Controleur::getTexte() {
    std::ostringstream ss;
    ss << _inventaire;

    return ss.str();
}


void Controleur::chargerInventaire(const std::string filename) {

    std::ifstream fichier(filename);
    std::string ligne;
    std::string texte;

    if (fichier.is_open()) {

        while (getline(fichier, ligne))
        {
            Bouteille b;
            fichier >> b;
            _inventaire._bouteilles.push_back(b);

        }

        for (auto &v : _vues) {
            v->actualiser();
        }
    }


}
