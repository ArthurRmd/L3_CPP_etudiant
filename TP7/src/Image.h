//
// Created by Arthur on 09/04/2020.
//

#ifndef TP_VIDE_IMAGE_H
#define TP_VIDE_IMAGE_H

class Image  {

private:
    int _largeur;
    int _hauteur;
    int *_pixels;

public:
    Image(int largeur, int hauteur);
    Image(const Image& img);
    int getLargeur() const;
    int getHauteur() const;
    int &getPixel(int i, int j) const;
    void setPixel(int i, int j, int couleur);
    ~Image();
    void ecrirePnm(const Image &img, const std::string &nomFichier);
    Image bordure(Image img, int couleur, int epaisseur);
    void remplir(Image &img);
    Image& operator=(const Image& image);


};


#endif //TP_VIDE_IMAGE_H
