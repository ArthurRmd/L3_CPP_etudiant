//
// Created by Arthur on 09/04/2020.
//

#include <iostream>
#include "Image.h"
#include <fstream>      // std::ofstream
#include <string>

using namespace std;

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur) {
    _pixels = new int[largeur * hauteur];
}

Image::Image(const Image &image) {
    _largeur = image._largeur;
    _hauteur = image._hauteur;
    _pixels = new int[_largeur * _hauteur];
    for (int i = 0; i < _largeur * _hauteur; ++i) {
        _pixels[i] = i;
    }
}


int Image::getLargeur() const {
    return this->_largeur;
}

int Image::getHauteur() const {
    return this->_hauteur;
}

int &Image::getPixel(int i, int j) const {
    return _pixels[(_largeur *j + i)];
}

void Image::setPixel(int i, int j, int couleur) {
    _pixels[(_largeur * j + i)] = couleur;
}

Image::~Image() {
    delete[] _pixels;
}

Image Image::bordure(Image img, int couleur, int epaisseur) {

    Image picture(img);

    for (int j = 0; j < epaisseur; ++j) {

        for (int i = 0; i < img.getLargeur(); ++i) {
            picture.setPixel(j, i, 110);
        }

    }

    return picture;

}

void Image::ecrirePnm(const Image &img, const std::string &nomFichier) {
    std::ofstream file(nomFichier);

    if (file.is_open()) {

        file << "P2 " << img.getLargeur() << " " << img.getHauteur() << " 255\n";

        for (int i = 0; i < img.getLargeur(); ++i) {

            for (int j = 0; j < img.getHauteur(); ++j) {
                file << img.getPixel(i,j) << " ";
            }
            file << "\n";
        }
        file.close();

    } else {
        throw std::string("Erreur");
    }
}

void Image::remplir(Image &img) {

    int color = 255;

    for (int i = 0; i < img.getLargeur(); ++i) {
        for (int j = 0; j < img.getHauteur(); ++j) {
            img.setPixel(i,j, color);
        }

        color = (255 / img.getLargeur()) * i;
    }
}

Image &Image::operator=(const Image &image) {

    _largeur = image._largeur;
    _hauteur = image._hauteur;
    int numberOfPixels = image._largeur * image._hauteur;

    _pixels = new int[numberOfPixels];

    for (int i = 0; i < numberOfPixels ; ++i) {
        _pixels[i] = i;
    }

    return *this;
}

