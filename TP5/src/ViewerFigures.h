//
// Created by Arthur on 09/04/2020.
//

#ifndef L3_CPP_TP5_VIEWERFIGURES_H
#define L3_CPP_TP5_VIEWERFIGURES_H

#include <gtkmm.h>
#include "ZoneDessin.h"

class ViewerFigures {

private:
    Gtk::Main _kit;
    Gtk::Window _window;
    ZoneDessin zoneDessin;

public:
    ViewerFigures(int argc, char **argv);
    void run();

};

#endif //L3_CPP_TP5_VIEWERFIGURES_H
