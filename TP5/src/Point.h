//
// Created by Arthur on 23/03/2020.
//

#ifndef TP_VIDE_POINT_HPP
#define TP_VIDE_POINT_HPP
#include <iostream>

using namespace std;

struct Point {
    int _x;
    int _y;

    Point(){}

    Point(int x, int y) {
        _x = x;
        _y = y;
    }

    string afficher() const{
        return to_string(_x) + "_" + to_string(_y);
    }
};

#endif //TP_VIDE_POINT_HPP
