//
// Created by Arthur on 09/04/2020.
//

#include "ZoneDessin.h"
#include "Ligne.h"
#include "PolygoneRegulier.h"

using namespace std;

ZoneDessin::ZoneDessin() {

    Couleur couleurLigne(1, 1, 0);
    Couleur couleurPolygone(0, 1, 1);

    _figures = {
            new Ligne(couleurLigne, Point(0, 0), Point(90, 100)),
            new Ligne(couleurLigne, Point(100, 50), Point(150, 250)),
            new PolygoneRegulier(couleurPolygone, Point(200, 400), 80, 36),
            new PolygoneRegulier(couleurPolygone, Point(100, 350), 120, 8)
    };

    add_events(Gdk::BUTTON_PRESS_MASK);

    signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));

};

ZoneDessin::~ZoneDessin() {

    for (FigureGeometrique *fG : _figures) delete fG;

}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> &context) {
    for (FigureGeometrique *fG : _figures) fG->afficher(context);

}

bool ZoneDessin::gererClic(GdkEventButton *event) {

    if (event->type == GDK_BUTTON_PRESS) {

        if (event->button == 1) {

            _figures.push_back(
                    new PolygoneRegulier(
                            Couleur(rand() % 101 / 100.f, rand() % 101 / 100.f, rand() % 101 / 100.f),
                            Point(event->x, event->y),
                            rand() % 100, rand() % 10)
                            );


        } else if (event->button == 3) {

            FigureGeometrique *fg = _figures.back();
            _figures.pop_back();
            delete fg;

        }

        get_window()->invalidate(false);
    }

    return true;
}