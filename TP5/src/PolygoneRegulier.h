//
// Created by Arthur on 23/03/2020.
//

#ifndef TP4_POLYGONEREGULIER_HPP
#define TP4_POLYGONEREGULIER_HPP

#include "FigureGeometrique.h"
#include "Point.h"
#include <vector>


class PolygoneRegulier : public FigureGeometrique {
private:
    int _nbPoints;
    vector <Point> _points;

public:
    PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes);
    int getNbPoints() const;
    const Point &getPoint(int indice) const;

    virtual void afficher(const Cairo::RefPtr<Cairo::Context> context) const override;


};

#endif //TP_VIDE_POLYGONEREGULIER_H
