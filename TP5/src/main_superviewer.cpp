//
// Created by Arthur on 09/04/20.
//

#include <gtkmm.h>
#include "ViewerFigures.h"

int main(int argc, char *argv[]) {

    ViewerFigures vf(argc, argv);

    vf.run();

    return 0;
}