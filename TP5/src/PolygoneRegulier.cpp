//
// Created by Arthur on 23/03/2020.
//

#include <cmath>
#include <iostream>
#include "PolygoneRegulier.h"
#include "Point.h"

using namespace std;

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes) :
FigureGeometrique(couleur), _nbPoints(nbCotes) {

    for (int i = 0; i < _nbPoints; ++i) {

        float theta = i * 2 * M_PI / (float) _nbPoints;

        int x = rayon * cos(theta) + centre._x;
        int y = rayon * sin(theta) + centre._y;

        _points.push_back(Point(x, y));

    }

}


void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> context) const {

    std::cout << "Polygone : " << _nbPoints << " points, voici les points :" << std::endl;
    for (int i = 0; i < _nbPoints; ++i) {
        _points[i].afficher();
    }

    for (int i = 0; i < _nbPoints; ++i) {

        context->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
        context->set_line_width(5.0);

        context->move_to(_points[i]._x, _points[i]._y);

        if (i + 1 == _nbPoints) {
            context->line_to(_points[0]._x, _points[0]._y);
            continue;
        }

        context->line_to(_points[i + 1]._x, _points[i + 1]._y);
    }

    context->stroke();

}


int PolygoneRegulier::getNbPoints() const {
    return _nbPoints;
}

const Point &PolygoneRegulier::getPoint(int indice) const {
    return _points[indice];
}
