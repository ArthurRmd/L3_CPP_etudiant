//
// Created by Arthur on 09/04/2020.
//

#ifndef L3_CPP_TP5_ZONEDESSIN_H
#define L3_CPP_TP5_ZONEDESSIN_H

#include <gtkmm.h>
#include "FigureGeometrique.h"

using namespace std;

class ZoneDessin : public Gtk::DrawingArea {

private:
    vector<FigureGeometrique *> _figures;

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context> &context) override;
    bool gererClic(GdkEventButton *event);

public:
    ZoneDessin();
    ~ZoneDessin();

};

#endif //L3_CPP_TP5_ZONEDESSIN_H
