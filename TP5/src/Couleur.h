//
// Created by Arthur on 23/03/2020.
//

#ifndef TP_VIDE_COULEUR_H
#define TP_VIDE_COULEUR_H

#include <iostream>

struct Couleur {
    double _r;
    double _g;
    double _b;

    Couleur(double r, double g, double b) {
        _r = r;
        _g = g;
        _b = b;
    }

    std::string afficher() const {
        return std::to_string(_r) + "_" + std::to_string(_g) + "_" + std::to_string(_b);
    }
};

#endif //TP_VIDE_COULEUR_HPP
