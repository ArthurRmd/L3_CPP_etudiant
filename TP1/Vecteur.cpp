
#include <iostream>
#include "Vecteur.hpp"
#include <cmath>
using namespace std;



Vecteur::Vecteur(float f1, float f2, float f3) {
    this->f1 = f1;
    this->f2 = f2;
    this->f3 = f3;
}

void Vecteur::afficher(){
    cout << this->f1 << endl;
    cout << this->f2 << endl;
    cout << this->f3 << endl;

}

float Vecteur::norme(){
    return sqrt(f1*f1 + f2*f2 + f3*f3);
}