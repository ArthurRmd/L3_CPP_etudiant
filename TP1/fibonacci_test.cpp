
#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_fibonacci_1) {  // premier test


    CHECK_EQUAL(2, fibonacciIteratif(3));
    CHECK_EQUAL(3, fibonacciIteratif(4));
    CHECK_EQUAL(5, fibonacciIteratif(5));
    CHECK_EQUAL(8, fibonacciIteratif(6));
}


TEST(GroupFibonacci, test_fibonacci_2) {  // premier test

    CHECK_EQUAL(2, fibonacciRecursif(3));
    CHECK_EQUAL(3, fibonacciRecursif(4));
    CHECK_EQUAL(5, fibonacciRecursif(5));
    CHECK_EQUAL(8, fibonacciRecursif(6));

}

