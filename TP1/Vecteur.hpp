 
#ifndef VECTEUR
#define VECTEUR


struct Vecteur
{
    float f1, f2, f3;
    Vecteur(float f1, float f2, float f3);
    void afficher();
    float norme();

};


#endif
