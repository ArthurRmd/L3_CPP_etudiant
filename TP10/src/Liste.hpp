
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
private:

    struct Noeud {
        int _valeur;
        Noeud *_ptrNoeudSuivant;
    };

    Noeud *_ptrTete;

public:

    class iterator {

        Noeud *_ptrNoeudCourant;

    public:

        iterator(Noeud *ptrNoeudCourant) {
            this->_ptrNoeudCourant = ptrNoeudCourant;
        }

        const iterator &operator++() {
            return this->_ptrNoeudCourant->_ptrNoeudSuivant;
        }

        int &operator*() const {
            return this->_ptrNoeudCourant->_valeur;

        }

        bool operator!=(const iterator &iter) const {
            return this->_ptrNoeudCourant != iter._ptrNoeudCourant;
        }

        friend Liste;
    };


public:

    Liste() {
        this->_ptrTete = nullptr;
    }

    ~Liste() {
        clear();
        delete _ptrTete;
    }

    void push_front(int value) {
        Noeud *newNoeud = new Noeud;

        newNoeud->_valeur = value;
        newNoeud->_ptrNoeudSuivant = this->_ptrTete;

        this->_ptrTete = newNoeud;
    }

    int &front() const {
        return this->_ptrTete->_valeur;
    }

    void clear() {

        while (this->_ptrTete != nullptr) {
            Noeud *noeud = this->_ptrTete;
            this->_ptrTete = this->_ptrTete->_ptrNoeudSuivant;
            delete noeud;
        }

    }

    bool empty() const {
        return this->_ptrTete == nullptr;
    }

    iterator begin() const {
        return this->_ptrTete;
    }

    iterator end() const {

        Noeud *noeudTemp = this->_ptrTete;

        while (noeudTemp->_ptrNoeudSuivant != nullptr) {
            noeudTemp = noeudTemp->_ptrNoeudSuivant;
        }

        return noeudTemp;
    }

};

std::ostream &operator<<(std::ostream &os, const Liste &) {
    return os;
}

#endif

