//
// Created by Arthur on 27/03/2020.
//

#include "Bibliotheque.h"

#include <iostream>
#include <algorithm>
#include <string>
#include <stdio.h>

using namespace std;

void Bibliotheque::afficher() const {

    cout << "Voici les livres de le bibliotheque" << endl;

    for (const Livre &livre: *this) {
        cout << livre << endl;
    }

}

void Bibliotheque::trierParAuteurEtTitre() {
    sort(begin(), end(), [](Livre livre_1, Livre livre_2) {
        return livre_1 < livre_2;
    });
}

void Bibliotheque::trierParAnnee() {
    sort(begin(), end(), [](Livre livre_1, Livre livre_2) {
        return livre_1.getAnnee() < livre_2.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const string &nomFichier) const {
    ofstream file(nomFichier);
    if (file.is_open()) {
        for (const Livre &livre : *this) file << livre << endl;
        file.close();
    } else {
        throw string("Probleme Fichier");
    }
}

void Bibliotheque::lireFichier(const string &nomFichier) {
    ifstream file(nomFichier);
    if (file.is_open()) {

        string line;

        while (getline(file, line)) {

            vector<string> book;
            size_t current, previous = 0;
            current = line.find(";");

            while (current != string::npos) {
                book.push_back(line.substr(previous, current - previous));
                previous = current + 1;
                current = line.find(";", previous);
            }

            book.push_back(line.substr(previous, line.length() - previous));

            push_back(Livre(book.at(0), book.at(1), stoi(book.at(2))));
        }
        file.close();
    } else {
        throw string("Probleme Fichier");
    }
}

