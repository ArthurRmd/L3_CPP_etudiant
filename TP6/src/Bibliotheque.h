//
// Created by Arthur on 27/03/2020.
//

#ifndef TP6_BIBLIOTHEQUE_H
#define TP6_BIBLIOTHEQUE_H


#include <vector>
#include "Livre.h"

class Bibliotheque : public std::vector<Livre> {

public:
    void afficher() const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void lireFichier(const std::string &nomFichier);
    void ecrireFichier(const std::string &nomFichier) const;

};


#endif //TP6_BIBLIOTHEQUE_H
