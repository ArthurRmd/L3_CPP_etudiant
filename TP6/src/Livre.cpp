//
// Created by Arthur on 27/03/2020.
//


#include <iostream>
#include <string>
#include "Livre.h"

using namespace std;

Livre::Livre() :_titre(""), _auteur(""), _annee(2020) {}

Livre::Livre(string titre, string auteur, int annee) :_titre(titre), _auteur(auteur), _annee(annee) {

    if (checkValide(titre) || checkValide(auteur)) {
        throw string("Erreur : titre et/ou autheur non valide");
    }

}

const string &Livre::getTitre() const {
    return _titre;
}

const string &Livre::getAuteur() const {
    return _auteur;
}

int Livre::getAnnee() const {
    return _annee;
}


bool Livre::checkValide(const string &content) {
    if (content.find(";") == string::npos || content.find("\n") == string::npos ) {
        return false;
    }
    return true;
}


ostream &operator<<(ostream &os, const Livre &livre) {
    os << livre._titre << " ; " << livre._auteur << " ; " << livre._annee;
    return os;
}

