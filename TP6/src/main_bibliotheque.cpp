
#include <iostream>
#include "Livre.h"
#include "Bibliotheque.h"

int main() {

    Livre livre1("livre_1", "Arthur", 2019);
    Livre livre2("livre_2", "Ocean", 2022);
    Livre livre3("livre_3", "Robert", 2021);

    Bibliotheque b1;

    b1.push_back(livre1);
    b1.push_back(livre3);
    b1.push_back(livre2);
    b1.trierParAuteurEtTitre();
    b1.afficher();


    Bibliotheque b2;

    b2.push_back(livre1);
    b2.push_back(livre3);
    b2.push_back(livre2);
    b2.trierParAuteurEtTitre();
    b2.afficher();

    return 0;
}

