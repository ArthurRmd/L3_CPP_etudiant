//
// Created by Arthur on 27/03/2020.
//

#ifndef TP6_LIVRE_H
#define TP6_LIVRE_H

#include <string>
#include <fstream>
#include <vector>
#include <ostream>


class Livre {

private:
    std::string _titre;
    std::string _auteur;
    int _annee;


public:
    Livre();
    Livre(std::string titre, std::string auteur, int annee);

    const std::string &getTitre() const;
    const std::string &getAuteur() const;
    int getAnnee() const;

    static bool checkValide(const std::string &content);

    bool operator<(const Livre &livre_2) const {
        if (_titre < livre_2.getTitre() || _auteur < livre_2.getAuteur()) {
            return true;
        }
        return false;
    }

    bool operator==(const Livre &livre_2) const {
        if( (_titre == livre_2.getTitre()) && (_auteur == livre_2.getAuteur()) && (_annee == livre_2.getAnnee())) {
            return true;
        }
        return false;
    }

    friend std::ostream &operator<<(std::ostream &os, const Livre &livre);

};

#endif //TP6_LIVRE_H
